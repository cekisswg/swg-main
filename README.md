### theswgsource Star wars Galaxies NGE dsrc/src repo

Note: This repository is still a work in progress, as is this README. 
If you run into any issues while setting up your server or environment you can contact us at www.swgsource.com

This repository houses the source code to the actual game server and other various server applications for Star Wars Galaxies NGE.  
It also contains source code to the game files (such as the client and server game data).

Credit: Stella Bellum dev team, swgmasters, reborn, folks at swgsource.com, SOE, Lucasarts.

### Environment Setup Guide

The source code can be compiled using our custom v1.1 VM.
Download it here: https://www.swgsource.com
